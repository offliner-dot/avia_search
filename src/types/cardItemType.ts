export type CardItemType = {
    depCity:string
    depAirport: string
    depIataCode:string
    arrivalCity:string
    arrivalAirport:string
    arrivalIataCode:string
    depDate:string
    duration:number
    arrivalDate :string
    carrier:string
    transfer: string
}