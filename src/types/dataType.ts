export type FlightType = {
    carrier: string
    passengerType : string
    feeAndTaxes : string
    transferTo: string
    transferFrom: string
    to: {
        airline: string
        arrivalAirport: string
        arrivalCity: {
            caption: string
            uid: string
        }
        arrivalDate: string
        departureAirport: string
        departureCity: {
            caption: string
            uid: string
        }
        departureDate: string
    }
    from: {
        airline: string
        arrivalAirport: string
        arrivalCity: {
            caption: string
            uid: string
        }
        arrivalDate: string
        departureAirport: string
        departureCity: {
            caption: string
            uid: string
        }
        departureDate: string
    }
    flightToken: string
    transferDurationTo: number
    transferDurationFrom : number
}