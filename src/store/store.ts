import {applyMiddleware, combineReducers, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import dataFlightReducer from "./reducers/dataFlightReducer";
import thunk from "redux-thunk";


const rootReducer = combineReducers({
    dataFlight : dataFlightReducer,
});

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
export type rootStore = ReturnType<typeof rootReducer>;