import json from "../../data/flights.json";

const FETCH = "FETCH";
const FETCH_SUCCESS = "FETCH_SUCCESS";
const FETCH_ERROR = "FETCH_ERROR";
const SORT_DATA_ON_PRICE = "SORT_DATA_ON_PRICE";

const initialState = {
    data: [],
    isFetch: false,
    errorMessage: "",
    sortData: []
};

const dataFlightReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH:
            return {
                ...state, isFetch: true
            }
        case FETCH_SUCCESS:
            return {
                ...state, data: [...action.data], sortData: [...action.data], isFetch: false
            }
        case FETCH_ERROR:
            return {
                ...state, errorMessage: action.message, isFetch: false
            }
        case SORT_DATA_ON_PRICE:
            return {
                ...state, sortData: [...action.data]
            }
        default:
            return state
    }
};


const fetch = () => ({type: FETCH});
const fetchSuccess = (data) => ({type: FETCH_SUCCESS, data});
const fetchError = (message) => ({type: FETCH_ERROR, message});
export const sortDataOnPrice = (data) => ({type: SORT_DATA_ON_PRICE, data});

export const initialDataThunk = () => async (dispatch) => {
    try {
        dispatch(fetch());
        let sortArr = [];
        let data = {};
        json.result.flights.forEach(item => {
            data.carrier = item.flight.carrier.caption;
            data.transferTo = item.flight.legs[0].segments.length;
            data.transferDurationTo = item.flight.legs[0].duration;
            data.transferDurationFrom = item.flight.legs[1].duration;
            data.transferFrom = item.flight.legs[1].segments.length;
            data.to = auxFuncGetInfo(item, data.transferTo, 0);
            data.from = auxFuncGetInfo(item, data.transferFrom, 1);
            data.passengerType = item.flight.price.passengerPrices[0].passengerType.uid
            data.feeAndTaxes = item.flight.price.passengerPrices[0].total.amount.split(".")[0]
            data.flightToken = item.flightToken
            sortArr.push({...data})
        })
        dispatch(fetchSuccess(sortArr));
    } catch (e) {
        dispatch(fetchError("Произошла ошибка"));
    }
}
export const sortDataOnPriceThunk = info => (dispatch, getState) => {
    let {data} = getState().dataFlight
    const temp = JSON.parse(JSON.stringify(data))
    if (info.key === "1") {
        temp.sort((a, b) => +a.feeAndTaxes > +b.feeAndTaxes ? 1 : -1)
    } else if (info.key === "2") {
        temp.sort((a, b) => +a.feeAndTaxes < +b.feeAndTaxes ? 1 : -1)
    }
    dispatch(sortDataOnPrice(temp))
}

export default dataFlightReducer;

function auxFuncGetInfo(item, transfer, direction = 0) {
    if (transfer > 1) {
        return {
            airline: item.flight.legs[direction].segments[0].airline.caption,
            departureAirport: item.flight.legs[direction].segments[0].departureAirport?.caption,
            departureCity: {
                caption: item.flight.legs[direction].segments[0].departureCity?.caption,
                uid: item.flight.legs[direction].segments[0].departureCity?.uid
            },
            departureDate: item.flight.legs[direction].segments[0].departureDate,
            arrivalAirport: item.flight.legs[direction].segments[1].arrivalAirport?.caption,
            arrivalCity: {
                caption: item.flight.legs[direction].segments[1].arrivalCity?.caption,
                uid: item.flight.legs[direction].segments[1].arrivalCity?.uid
            },
            arrivalDate: item.flight.legs[direction].segments[1].arrivalDate,
        }
    } else {
        return {
            airline: item.flight.legs[direction].segments[0].airline.caption,
            departureAirport: item.flight.legs[direction].segments[0].departureAirport?.caption,
            departureCity: {
                caption: item.flight.legs[direction].segments[0].departureCity?.caption,
                uid: item.flight.legs[direction].segments[0].departureCity?.uid
            },
            departureDate: item.flight.legs[direction].segments[0].departureDate,
            arrivalAirport: item.flight.legs[direction].segments[0].arrivalAirport?.caption,
            arrivalCity: {
                caption: item.flight.legs[direction].segments[0].arrivalCity?.caption,
                uid: item.flight.legs[direction].segments[0].arrivalCity?.uid
            },
            arrivalDate: item.flight.legs[direction].segments[0].arrivalDate,
        }
    }
}
