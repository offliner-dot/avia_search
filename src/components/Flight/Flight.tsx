import React, {FC} from 'react';
import moment from "moment";
import 'moment/locale/ru'
import {FlightType} from "../../types/dataType";
import style from "./Flight.module.css";
import CardItem from "../Card/CardItem";
import {CardItemType} from "../../types/cardItemType";


type PropsType = {
    flight: FlightType
}
const Flight: FC<PropsType> = ({flight}) => {
    console.log()
    const to: CardItemType = {
        depCity: flight.to.departureCity.caption,
        depAirport: flight.to.departureAirport,
        depIataCode: flight.to.departureCity.uid,
        arrivalCity: flight.to.arrivalCity.caption,
        arrivalAirport: flight.to.arrivalAirport,
        arrivalIataCode: flight.to.arrivalCity.uid,
        depDate: flight.to.departureDate,
        duration: flight.transferDurationTo,
        arrivalDate: flight.to.arrivalDate,
        carrier: flight.carrier,
        transfer: flight.transferTo,
    };
    const from: CardItemType = {
        depCity: flight.from.departureCity.caption,
        depAirport: flight.from.departureAirport,
        depIataCode: flight.from.departureCity.uid,
        arrivalCity: flight.from.arrivalCity.caption,
        arrivalAirport: flight.from.arrivalAirport,
        arrivalIataCode: flight.from.arrivalCity.uid,
        depDate: flight.from.departureDate,
        duration: flight.transferDurationFrom,
        arrivalDate: flight.from.arrivalDate,
        carrier: flight.carrier,
        transfer: flight.transferFrom,
    };
    const formatDate = (str: string, options: string) => {
        let date = new Date(Date.parse(str));
        return moment(date).format(options);
    };
    const getTimeFromMins = (mins: number) => {
        let hours = Math.trunc(mins / 60);
        let days = Math.trunc(hours / 24)
        if (hours > 23) hours = hours % 24
        let minutes = mins % 60;
        return days ? `${days}д ${hours}ч ${minutes}м` : hours ? `${hours}ч ${minutes}м` :
            `${minutes}м`;
    };
    return (
        <div className={style.card}>
            <div className={style.cardWrapper}>
                <div className={style.title}>
                    <div className={style.carrier}>{flight.carrier}</div>
                    <div className={style.priceInfo}>
                        <div className={style.priceTaxes}>{flight.feeAndTaxes} ₽</div>
                        <div className={style.priceAdult}>Стоимость для одного взрослого пассажира</div>
                    </div>
                </div>
                <CardItem data={to} formatDate={formatDate} getTimeFromMins={getTimeFromMins}/>
                <hr/>
                <CardItem data={from} formatDate={formatDate} getTimeFromMins={getTimeFromMins}/>
            </div>
            <div className={style.button}>Выбрать</div>
        </div>
    );
};


export default Flight;