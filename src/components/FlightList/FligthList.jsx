import React, {useEffect, useState} from 'react';
import 'moment/locale/ru'
import {useTypedSelector} from "../../hooks/useTypedSelector";
import Flight from "../Flight/Flight";
import {Pagination} from 'antd';

const FlightList = () => {
    const {sortData} = useTypedSelector(state => state.dataFlight);
    const [flights, setFlights] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [flightsPerPage, setFlightsPerPage] = useState(2)
    useEffect(() => {
        setFlights(sortData)
    }, [sortData]);


    // Get current flights
    const indexOfLastPost = currentPage * flightsPerPage;
    const indexOfFirstPost = indexOfLastPost - flightsPerPage;
    const currentFlights = flights.slice(indexOfFirstPost, indexOfLastPost);

    const onChange = (page, perPage) => {
        setCurrentPage(page);
        setFlightsPerPage(perPage);
    };
    return (
        <div>
            {currentFlights.map((item) => (
                <Flight key={item.flightToken} flight={item}/>
            ))}
            <Pagination defaultCurrent={1} total={sortData.length} onChange={onChange} defaultPageSize={flightsPerPage}
                        pageSizeOptions={["2", "10", "20", "50", "100"]}/>
        </div>
    );
};

export default FlightList;