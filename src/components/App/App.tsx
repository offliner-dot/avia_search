import React, {useEffect, useState} from 'react';
import {Layout, Menu} from 'antd';
import {MenuFoldOutlined, MenuUnfoldOutlined, SortAscendingOutlined, SortDescendingOutlined} from '@ant-design/icons';
import 'antd/dist/antd.css';
import {useDispatch} from "react-redux";
import {initialDataThunk, sortDataOnPriceThunk} from "../../store/reducers/dataFlightReducer";
import "./app.css"
import FlightList from "../FlightList/FligthList";


function App() {
    const {Header, Sider, Content} = Layout;
    const [collapsed, setCollapsed] = useState(false)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(initialDataThunk())
    }, []);
    const toggle = () => {
        setCollapsed(!collapsed);
    };
    const sortOnPrice = (info: any) => {
        dispatch(sortDataOnPriceThunk(info))
    }
    return (
        <Layout>
            <Sider trigger={null} collapsible collapsed={collapsed} width={300}>
                <div className="logo"/>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1" icon={<SortDescendingOutlined/>} onClick={(info) => sortOnPrice(info)}>
                        Сортировать по возрастанию цены
                    </Menu.Item>
                    <Menu.Item key="2" icon={<SortAscendingOutlined/>} onClick={(info) => sortOnPrice(info)}>
                        Сортировать по убыванию цены
                    </Menu.Item>
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background" style={{padding: 0}}>
                    {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: toggle,
                    })}
                </Header>
                <Content
                    className="site-layout-background"
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                    }}
                >
                    <FlightList/>
                </Content>
            </Layout>
        </Layout>
    );
}

export default App;
