import React, {FC} from 'react';
import style from "./cardItem.module.css";
import {FieldTimeOutlined} from "@ant-design/icons";
import {CardItemType} from "../../types/cardItemType";

type PropsType = {
    data: CardItemType
    getTimeFromMins: (duration: number) => string
    formatDate: (date: string, option: string) => string
}
const CardItem: FC<PropsType> = ({data, formatDate, getTimeFromMins}) => {
    return (
        <div className={style.flightInfo}>
            <div className={style.route}>
                {data.depCity}, {data.depAirport}
                ({<a href="#">{data.depIataCode}</a>}) <span className={style.arrow}>&#10230;</span>
                {data.arrivalCity}, {data.arrivalAirport}
                ({<a href="#">{data.arrivalIataCode}</a>})
            </div>
            <div className={style.timeInfo}>
                <div className={style.departedTime}>
                    {formatDate(data.depDate, "HH:mm")}
                    <div className={style.departedDate}>
                        {formatDate(data.depDate, "DD MMM dd")}
                    </div>
                </div>
                <div className={style.durationFlight}>
                    <FieldTimeOutlined/> {getTimeFromMins(data.duration)}
                </div>
                <div className={style.arrivalTime}>
                    <div className={style.arrivalDate}>
                        {formatDate(data.arrivalDate, "DD MMM dd")}
                    </div>
                    {formatDate(data.arrivalDate, "HH:mm")}
                </div>
            </div>
            <div className={style.transfer}>
                {Number(data.transfer) > 1 && <span>1 пересадка</span>}
            </div>
            <div className={style.carrierInfo}>
                Рейс выполняет: {data.carrier}
            </div>
        </div>
    );
};

export default CardItem;